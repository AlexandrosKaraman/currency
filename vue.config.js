module.exports = {
  runtimeCompiler: true,
  transpileDependencies: [
    'vuetify'
  ],
  css: {
    loaderOptions: {
      scss: {
        preventData: ' @import "@/assets/styles.sass"; '
      }
    }
  }
}
