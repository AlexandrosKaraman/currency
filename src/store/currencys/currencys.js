export default {
  state: {
    currencys: [
      {
        id: 1,
        name: "UAH",
        rate: 1,
        date: "04.02.2022",
      },
      {
        id: 2,
        name: "USD",
        rate: 28,
        date: "04.02.2022",
      },
      {
        id: 3,
        name: "EUR",
        rate: 30,
        date: "04.02.2022",
      },
    ],
  },
  actions: {
    updateCurrency({ commit }, { rate, id }) {
      commit("updateCurrency", {
        rate,
        id,
      });
    },
  },
  mutations: {
    updateCurrency(state, { rate, id }) {
      const currency = state.currencys.find((currency) => {
        return currency.id === id;
      });
      currency.rate = rate;
    },
  },
  getters: {
    currencys(state) {
      return state.currencys;
    },
  },
};
