import Vue from "vue";
import Vuex from "vuex";

import currencys from "./currencys/currencys";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { currencys },
});
