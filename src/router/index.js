import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    meta: { layout: "footer" },
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/currencys",
    name: "currencys",
    meta: { layout: "main" },
    component: () => import("../views/Currencys.vue"),
  },
  {
    path: "/history",
    name: "history",
    meta: { layout: "main" },
    component: () => import("../views/History.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
